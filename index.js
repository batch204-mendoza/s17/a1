//console.log("hello world");


/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function personDetailSFunc(){
		let fullName= prompt("whats your full name?");
		let age= prompt("How old are you?");
		let completeAdress= prompt("whats your full address?");
		console.log("hello "+ fullName );
		console.log("you are "+ age+" years old" );
		console.log("you live in "+ completeAdress );
	}
	personDetailSFunc();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function topFiveMusic(){
		let top1 = "Audioslave"
		let top2 = "Sabaton"
		let top3 = "Colm Mcguiness"
		let top4 = "LP"
		let top5 = "Paramore"
		console.log("1. " +top1)
		console.log("2. " +top2)
		console.log("3. " +top3)
		console.log("4. " +top4)
		console.log("5. " +top5)
	}
	topFiveMusic()

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function topFiveMovies(){
		let top1 = "Reservoir dogs";
		let top1Rating="99.8%";
		let top2 = "inception";
		let top2Rating="97.8%";
		let top3 = "Lobster";
		let top3Rating="96.5%";
		let top4 = "shoot em up";
		let top4Rating="90.8%";
		let top5 = "Ang Pangarap kong holdup";
		let top5Rating="100%";
		console.log("1. " +top1);
		console.log("Rotten Tomatoes rating: "+ top1Rating);
		console.log("2. " +top2);
		console.log("Rotten Tomatoes rating: "+ top2Rating);
		console.log("3. " +top3);
		console.log("Rotten Tomatoes rating: "+ top3Rating);
		console.log("4. " +top4);
		console.log("Rotten Tomatoes rating: "+top4Rating);
		console.log("5. " +top5);
		console.log("Rotten Tomatoes rating: "+top5Rating);
	}
	topFiveMovies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

